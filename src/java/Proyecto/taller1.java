
package Proyecto;

import javax.inject.Named;
import javax.enterprise.context.Dependent;


@Named(value = "taller1")
@Dependent
public class taller1 {

       private String NOMBRE ="Alexis Correa";

    /**
     * Get the value of NOMBRE
     *
     * @return the value of NOMBRE
     */
    public String getNOMBRE() {
        return NOMBRE;
    }

    /**
     * Set the value of NOMBRE
     *
     * @param NOMBRE new value of NOMBRE
     */
    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public taller1() {
    }
    
}
